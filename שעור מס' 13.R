install.packages('RSQLite')
install.packages('sqldf')
library(RSQLite)
library(sqldf)
#אנחנו רוצים למצוא את כל הרשומות שבהן השפיס הוא סטוסה 
str(iris)
levels(iris$Species)

iris$Petal_Length <- iris$Petal.Length
setosa_large <- sqldf('
                      SELECT * FROM iris i1
                      WHERE I1.Species = "setosa"
                      AND i1.Petal_Length > 1.3
                
                      ')
