redwine<-read.csv("redwine.csv",header=TRUE,sep=";")
str(redwine)

whitewine<-read.csv("whitewine.csv",header=TRUE,sep=";")
str(whitewine)


View(redwine)
#1 make avector with all values and add column to each data frame
#---------FOR THE RED WINE-------------------
nrow(redwine)
winetype<- vector(,nrow(redwine))

Winecolor <- function(winetype){
  if(winetype==FALSE){
    return('redwine')
  }else{
    return (winetype)
  }
}
#return a vector  by sapply 
winetype <- sapply(winetype, Winecolor)
str(winetype)
redwine$Winecolor <-winetype 

#---------FOR THE WHITE WINE-------------------
nrow(whitewine)

winetype2 <- vector(,nrow(whitewine))
str(winetype2)

Winecolor2 <- function(winetype2){
  if(winetype2==FALSE){
    return('whitewine')
  }else{
    return (winetype2)
  }
}
#return a vector  by sapply function
winetype2 <- sapply(winetype2, Winecolor2)
str(winetype2)
whitewine$Winecolor <-winetype2 



#2 merge the 2 dataframes
wine <- merge(redwine,whitewine, all= TRUE)
str(wine)
View(wine)

#3 show histogram of residual.sugar 
library(ggplot2)
##residual.sugar - histogram :  The Red wine has higher sugar value
pl1 <- ggplot(wine, aes(x= residual.sugar))
pl1 <- pl1 + geom_histogram(binwidth=0.5, aes(fill = Winecolor))+xlim(0, 40)+ ggtitle('A histogram of residual.suga')  

### alcohol level - histogram : The Red wine has higher alcohol value
pl2 <- ggplot(wine, aes(x= wine$alcohol))
pl2 <- pl2 + geom_histogram(binwidth=0.5, aes(fill = Winecolor))+ ggtitle('A histogram of alcohol')  

#4-------------K means model-------------------
any(is.na(wineClaster$cluster))
wineClaster <- kmeans(wine[,1:12],3 , nstart = 20) 
wineClaster$cluster
table(wineClaster$cluster,wine$Winecolor)
dim(wine)
length(wineClaster$cluster)
library(cluster)

clusplot(wine,wineClaster$cluster, color=T, shade=T, labels=0, lines=0)


