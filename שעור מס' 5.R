bike <- read.csv("train.csv")
str(bike)


#change to character
bike$datetime <-as.character(bike$datetime)

#then we split date and time by blank
bike$date <- sapply(strsplit(as.character(bike$datetime),' '),"[",1)
bike$hour <- sapply(strsplit(as.character(bike$datetime),' '),"[",2)

#we cast the date to date
bike$date <- as.Date(bike$date)

#we take the number from the bike
bike$hour <- sapply(strsplit(as.character(bike$hour),':'),"[",1)
bike$hour <- as.numeric(bike$hour)

#bike$time <- NULL

#factor season to given values
bike$season <- factor(bike$season, levels = c(1,2,3,4), labels = c('spring','summer', 'fall','winter'))

library(ggplot2)

#how temprature effects count. x equals temp, y equals count. if we want to change the color of all of the graph we add color = to the geom point. since we want to change the boldness of the color by temp we add aes
pl <- ggplot(bike,aes(temp,count)) + geom_point(alpha = 0.3, aes(color = temp))

#how date affects count
pl <- ggplot(bike, aes(date,count)) + geom_point(alpha=0.5,aes(color=temp))
pl + scale_color_continuous(low = 'blue', high = 'red')

# how season affects count
pl <- ggplot(bike, aes(season, count)) + geom_boxplot(aes(color=season))

# how hour affects count on weekdays. 1=sunday
pl <- ggplot(bike[bike$workingday != 1,],aes(hour, count))
pl <- pl +geom_point(position = position_jitter(w=0.5,h=0), aes(color=temp))

# how hour affects count on weekdays. 1=sunday. jitter spreads the dots so you could see all dots more clearly
pl <- ggplot(bike[bike$workingday == 1,],aes(hour, count))
pl <- pl +geom_point(position = position_jitter(w=0.5,h=0), aes(color=temp))

bike$datetime <- NULL 

#data preparing for linear model. we want to  predict count
#this will be our date to split
splitDate <- as.Date("2012-05-01")

dateFilter <- bike$date <= splitDate
# we will get bike when values are true in rows and will get all of columns - bike[what rows, what columns]
bike.train <- bike[dateFilter,]
bike.test <- bike[!dateFilter,]


#linear model works infront of target value= count ~ - regarding .=all rows minus atemp(etc)
model <- lm(count ~ .-atemp -casual -registered -date, bike.train)
summary(model)

#predict by model, count values by bike.train
predicted.train <- predict(model,bike.train)
predicted.test <- predict(model,bike.test)

MSE.train = mean((bike.train$count-predicted.train)^2)
MSE.test= mean((bike.test$count-predicted.test)^2)

RMSE.train <-MSE.train**0.5
RMSE.test <-MSE.test**0.5
#יש אובר פיטינג היות ויש פער גדול בין הטריין לטסט זאת אומרת שהמודל שלנו עובד טוב על הנתונים שהוא מכיר  מראש  ולעומת זאת בטסט הוא עובד פחות טוב